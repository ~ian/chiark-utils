.\" Hey, Emacs!  This is an -*- nroff -*- source file.
.TH SYNC\-ACCOUNTS 5 "15th July 2002" "Greenend" "chiark utilities"
.SH NAME
/etc/sync\-accounts \- configuration file for sync\-accounts
.SH DESCRIPTION
.B /etc/sync\-accounts
contains the default configuration of the
.BR sync-accounts (8)
account synchronisation tool.

The configuration file specifies how to access and update the local
password and group databases, where sync-accounts should log.

It also specifies the list of (remote) sources for account
information, and which accounts and details should be copied from each
source to the local system.
.SH OVERALL SYNTAX AND SEMANTICS
The configuration file is parsed as a series of lines.  First, leading
and trailing whitespace on each line is removed, and then empty lines,
or lines starting with
.BR # ,
are removed.

Each line is parsed as a directive.  The order of directives is
significant; some directives set up information which later
directives rely on.

The configuration file must contain an
.B end
directive; anything after that point is ignored.
.SH GLOBAL DIRECTIVES
These directives may appear only at the start of the file (before any
other directives), and each directive must appear only once;
otherwise, sync-accounts my behave oddly.
.TP
.BR lockpasswd | lockgroup " \fImethod\fP [\fIdetails \fP...]"
Specifies how the passwd and group files should be read and/or locked.
See
.B LOCKING METHOD DIRECTIVES
below.
.TP
.BI "logfile " filename
Append log messages to
.I filename
instead of stdout.
Errors still go to stderr.
.TP
.BR localformat " " bsd | std
Specifies the local password file is in the relevant format:
.B std
is the standard V7 password file (with a SysV-style
/etc/shadow if /etc/shadow exists).
.B bsd
is the BSD4.4 master.passwd format, and should be used only with
.BR "lockpasswd runvia vipw" .
The default is
.BR std .
.SH LOCKING METHOD DIRECTIVES
One
.B lockgroup
and one
.B lockpasswd
directive must be present, in the global directives at the start of
the config file.

The choice of the appropriate directives can be difficult without
special knowledge of the local system.  In general, it is best to use
.B lockpasswd runvia vipw
where this is available, as if this works avoids having to know the
names of the lockfiles.

GNU systems (including GNU/Linux and Debian GNU/BSD) typically lock
the group file separately and supply
.BR vigr ,
in which case you should use
.BR "lockgroup vigr" .

Most systems other than GNU do not lock the group file at all (or
assume that all programs which modify the group file will lock the
passwd file), in which case
.B lockgroup none
is appropriate.

If vigr or vipw is not available or is known to be broken (eg, because
it does not lock properly), then use
.BR link .
.TP
.BR lockpasswd | lockgroup " " runvia " \fIprogram\fP
sync-accounts will reinvoke itself using
.IR program ,
which must behave like
.B vipw
or
.BR vigr .
sync-accounts will set the
.B EDITOR
environment variable to the path it was invoked with (Perl's
.BR $0 )
and put some information for its own use into
.B SYNC_ACCOUNTS_*
environment variables (which will also allow sync-accounts to tell
that it has already been reinvoked via
.I program
and should not do so again).

If both
.BI "lockpasswd runvia " vipw
and
.BI "lockgroup runvia " vigr
are specified, then it must be possible and safe for the EDITOR
run by
.I vipw
to invoke
.IR vigr ,
as this is what sync-accounts will do.
.TP
.BR lockpasswd | lockgroup " " link " \fIsuffix\fP|\fIfilename\fP
sync-accounts will attempt to lock the passwd or group file by making
a hardlink from the real file to the specified filename.
If
.IR suffix | filename
starts with a
.B /
it is interpreted as a filename; otherwise it is interpreted as
a suffix, to be appended to the real database filename.
.TP
.BR lockpasswd | lockgroup " " none
sync-accounts will not attempt to lock the passwd or group files at
all.

.B lockgroup none
is appropriate on systems where there is no separate locking for the
group file (either because there is no proper support for automatic
editing of the group file, or because you're expected to lock the
password file), although in the absence of
.B vigr
it's inevitable that simultaneous changes to the group file made by
both the human sysadmin and by sync-accounts will cause problems.

.B lockpasswd none
is very dangerous and should not normally be used.  It will cause data
loss if any other tool for changing password data is used - eg,
.BR passwd (1).
.SH PER-SOURCE DIRECTIVES
Within each source's section, all of the per-source directives must
appear before any account-selection directives; otherwise
sync-accounts may behave oddly.  If a per-source directive is
repeated, the last setting takes effect.
.TP
.BI "host " source
Starts a source's section.  Usually each source will correspond
exactly to one host which is acting as a source of account data.
The
.B host
directive resets the per-source parameters to the defaults.
.I source
need not be the source host's official name in any sense and is used
only for identification.  Any
.I source
must be named in only one
.B host
directive, or sync-accounts may behave oddly.
.TP
.BR getpasswd | getgroup | getshadow " \fIcommand\fP..."
sync-accounts always fetches account data from sources by running specified
commands on the local host; it does not contain any network protocols,
itself.

.I command
is fed to
.BR "sh -c"
and might typically contain something like
.br
.B "    ssh syncacct@remote.host cat /etc/passwd"
.br
where the user syncacct on remote.host is in group shadow, or
.br
.B "    cat /var/local/sync-accounts/remote.host/passwd"
where the file named is copied across using cron.

.B getpasswd
must be specified if user data is to be transferred;
.B getgroup
must be specified if group data is to be transferred.

.B getshadow
should be specified iff getpasswd is specified but the data from
getpasswd does not contain actual password information, and should
emit data in Sys-V shadow password format.
.TP
.BR remoteformat " " std | bsd
Specifies the format of the output of getpasswd.
.B std
is standard V7 passwd file format (optionally augmented by the use of
a shadow file fetched with getshadow).
.B bsd
is the BSD4.4 master.passwd format (and getshadow should not normally
be used with
.BR "remoteformat bsd" ).
The default is
.BR std .
.SH SYNCHRONISATION SETTINGS
The following directives affect the way that account data is copied.
They may be freely mixed with other directives, and repeated.  The
setting in effect is the one set by the last relevant settings
directive before any particular account-selection directive.
.TP
.BR uidmin | uidmax " \fivalue\fP"
When an account is to be created locally, a uid/gid will be chosen
which is one higher than the highest currently in use, except that ids
below uidmin or above uidmax are ignored and will never be used.
There is no default.
.TP
.BI "homebase " homebase
When an account is to be created locally, its home directory will be
.IB homebase / username
where
.I username
is the name of the account.  The default is
.BR /home .
.TP
.RB [ no ] sameuid
Specifies whether uids are supposed to match.  With
.BR sameuid ,
it is an error for the uid or gid of a synchronised local account not
to match the corresponding remote account, and new local accounts will
get the remote accounts' ids.
The default is
.BR nosameuid .  
.TP
.BR usergroups " | " nousergroups " | " defaultgid " \fIgid\fP"
Specifies whether local accounts are supposed to have
corresponding groups, or all be part of a particular group.  The
default is
.BR usergroups .

With
.BR usergroups ,
when a new account is created, the
corresponding per-user group will be created as well, and
per-user groups are created for existing accounts if necessary
(if account creation is enabled).  If the gid or group name for
a per-user group is already taken for a different group name or
gid this will be logged, and processing of that account will be
inhibited, but it is not a fatal error.

With
.BR defaultgid ,
newly-created accounts will be made a part of that group,
and the groups of existing accounts will be left alone.

With
.BR nousergroups ,
no new accounts can be created, and existing accounts' groups will be
left alone.
.TP
.BR createuser " [\fIcommand\fP] | " nocreateuser
Specifies whether accounts found on the remote host should be created
if necessary, and what command to run to do the the rest of the
account setup (eg, creation of home directory, etc.).  The default is
.BR nocreateuser .

If
.B createuser
is specified without a command then
.B sync-accounts-createuser
is used; the supplied sync-accounts-createuser program is a reasonable
minimal implementation.

With
.BR createuser ,
either sameuid, or both uidmin and uidmax, must be specified, if
accounts are actually to be created.

The command is passed to
.BR "sh -c" .
See
.BR sync-accounts-createuser (8)
for details of
.IR command 's
environment and functionality.
.TP
.BR group | nogroup " \fIglob-pattern\fP"
.B group
specifies that the membership of the local groups specified should be
adjusted adjusted whenever account data for any user is copied, so
that the account will be a member of the affected group locally iff
the source account it is a member of the same group on the source
host.

The most recently-encountered glob-pattern for a particular group
takes effect.  The default is
.BR "nogroups *" .

The glob patterns may contain only alphanumerics, the two glob
metacharacters
.BR "* ?"
and four punctuation characters
.BR "- + . _" ;
\fB\\\fP-quoting and character sets and ranges are not supported.
.TP
.BI "defaultshell " pathname
Local accounts' shells will, when an account is synchronised, be set
to the remote account's shell if the same file exists locally and is
executable.  Otherwise, this value will be used.  The
default is
.BR /bin/sh .
.SH ACCOUNT SELECTION
These directives specify that the selected accounts are to be
synchronised: that is, the local account data will be unconditionally
overwritten (according to the synchronisation settings) with data from
the current source (according to the most recent
.B host
directive).

Any particular local username will only be synchronised once; the
source and settings for first account selection directive which
selects that local username will be used.

When an account is synchronised, the account password, comment field,
and shell will be copied unconditionally.  If
.B sameuid
is in effect specified the uid will be checked (or copied, for new
accounts).
.TP
.BR user " \fIusername\fP [" remote "=\fIremoteusername\fP]"
Specifies that account data should be copied for local user
.I username
from the remote account
.I remoteusername
(or
.I username
if
.I remoteusername
is not specified).
.TP
.RI "\fBusers\fP " ruidmin - ruidmax
Specifies that all remote users whose remote uid is in the given range
are to be synchronised to corresponding user accounts.  (Note that the
remote uid will only be copied if
.B sameuid
is in effect.)
.TP
.BI "nouser " username
Specifies that data for
.I username is not to be copied, even
if subsequent user or users directives suggest that it should be.
.TP
.B addhere
This directive has no effect on sync-accounts.  However, it is used as
a placeholder by grab-account: new accounts for creation are inserted
just before addhere.  See
.BR grab-account (8).
.SH FINAL DIRECTIVE
.TP
.B end
must appear in the configuration file, usually at the end of the file.
Nothing after it will be read.
.SH BUGS
The advice about the correct
.B lockpasswd
and
.B lockgroup
directives is probably out of date or flatly wrong.
.SH AUTHOR
.B sync-accounts
and this manpage are part of the
.B sync-accounts
package which was written by Ian Jackson <ian@chiark.greenend.org.uk>.
They are Copyright 1999-2000,2002 Ian Jackson
<ian@davenant.greenend.org.uk>, and Copyright 2000-2001 nCipher
Corporation Ltd.

The sync-accounts package is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3, or (at
your option) any later version.

This is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along
with this program; if not, consult the Free Software Foundation's
website at www.fsf.org, or the GNU Project website at www.gnu.org.
.SH SEE ALSO
.BR sync-accounts "(8), "
.BR grab-account "(8), "
.BR sync-accounts-createuser "(8), "
.BR passwd "(5), "
.BR group "(5), "
.BR shadow "(5), "
.BR master.passwd "(5), "
.BR vipw "(8), "
.BR vigr "(8)"
