.\" Hey, Emacs!  This is an -*- nroff -*- source file.
.TH CHIARK\-NAMED\-CONF 8 "12th January 2002" "Greenend" "chiark utilities"
.SH NAME
chiark\-named\-conf \- check and generate nameserver configuration
.SH SYNOPSIS
.BR chiark\-named\-conf " [\fIoptions\fP] " \-n | \-y | \-f
.br
\fBchiark\-named\-conf\fP [\fIoptions\fP] \fIzone ...\fP
.SH DESCRIPTION
.B chiark\-named\-conf
is a tool for managing nameserver configurations and checking for
suspected DNS problems.  Its main functions are to check that
delegations are appropriate and working, that secondary zones are
slaved from the right places, and to generate a configuration for
.BR BIND ,
from its own input file.

By default, for each zone, in addition to any warnings, the output
lists the zone's configuration type.  If the zone is checked, the
serial number at each of the nameservers is shown, with any
unpublished primary having
.B *
after the serial number.
.SH OPTIONS

.SS MODE OPTIONS
If one of the options
.BR -n ", " -y ", or " -f
is supplied then chiark-named-conf will read its main configuration
file for the list of relevant zones.  It will then check the
configuration and delegation for each zone
and/or generate and install a new configuration file for
the nameserver:
.TP
.BR \-y | \-\-yes
Generate and install new nameserver config, as well as checking
configuration, for all listed zones.
.TP
.BR \-n | \-\-no
Check configuration, for all listed zones, but
do not generate new nameserver config.
.TP
.BR \-f | \-\-force
Generate and install new nameserver config, without doing any
configuration cross-checking.  (Syntax errors in our input
configuration will still abort this operation.)
.TP
.BR \-\-nothing
Do nothing: do no checks, and don't write a new config.  This can be
used to get a list of the zones being processed.
.TP
.BR \-\-mail\-first " | " \-\-mail\-middle " | " \-\-mail\-final
Send mails to zone SOA MNAMEs reporting zones with problems.  You must
call chiark\-named\-conf at least twice, once with \-\-mail\-first,
and later with \-\-mail\-final, and preferably with one or more calls
to \-\-mail\-middle in between.  All three options carry out a check
and store the results; \-\-mail\-final also sends a mail to the zone
SOA MNAME or local administrator, if too many of the calls had errors
or warnings (calls before the most recent \-\-mail\-first being
ignored).
.TP
.B \-mail\-final\-test
just like \-\-mail\-final except that it always sends mail to the
local server admin and never to remote zone contacts, adding
.B (testing!)
to the start of the To: field.
.LP
Alternatively, one or more zone names may be supplied as arguments, in
which case their delegations will be checked, and compared with the
data for that zone in the main configuration (if any).  In this case
no new configuration file for the nameserver will be made.

.SS ADDITIONAL OPTIONS
.TP
.BR \-A | \-\-all
Checks even zones known to be broken.  Ie, ignores the
.B ?
zone style modifier in the configuration.
.TP
.BR \-C | \-\-config " \fIconfig\-file\fP"
Use
.I config\-file
instead of
.BR /etc/bind/chiark-conf-gen.zones .
Also changes the default directory.
.TP
.BR \-D
Enables debugging.  Useful for debugging chiark\-named\-conf, but
probably not useful for debugging your DNS configuration.  Repeat to
increase the debugging level.  (Maximum is
.BR -DD .)
.TP
.BR \-g | \-\-glueless
Do not warn about glueless referrals (strictly, makes the zone style
modifier
.B ~
the default).  Not recommended - see the section GLUELESSNESS, below.
.TP
.BR \-l | \-\-local
Only checks for mistakes which are the responsibility of the local
administrator (to fix or get fixed).  This means that for published
and stealth zones we only check that we're slaving from the right
place and that any names and addresses for ourself are right.  For
primary zones all checks are still done.  It is a mistake to specify
.B \-l
with foreign zones (zones supplied explictly on the command line but
not relevant to the local server); doing so produces a warning.
.TP
.BI \-m group !*$@~?
Overrides a
.B modifiers
directive in the configuration file.  The modifiers specified in the
directive are completely replaced by those specified in this command
line option.  (Note that modifiers specified in per-zone directives
still override these per-group settings.)  If more than one
.B modifiers
directive specifies the same group, they are all affected.
.B modifiers
directives which don't specify a group cannot be affected.  It is an
error if the group does not appear in the config file.  See ZONE STYLE
MODIFIERS, below.
.PP
The special group
.B foreign
is used for zones which don't appear in the configuration file.
.TP
.BR \-q | \-\-quiet
Suppress the usual report of the list of nameservers for each zone and
the serial number from each.  When specified twice, do not print any
information except warnings.
.TP
.BR \-r | \-\-repeat
When a problem is detected, warn for all sources of the same imperfect
data, rather than only the first we come across
.TP
.BR \-v | \-\-verbose
Print additional information about what is being checked, as we go
along.
.SH USAGE
The file
.B /etc/bind/chiark-conf-gen.zones
(or other file specified with the
.B \-C
option) contains a sequence of directives, one per line.  Blank lines
are permitted.  Leading and trailing whitespace on each line is
ignored.  Comments are lines starting with
.BR # .
Ending a line with a
.BR \\
joins it to the next line, so that long directives can be split across
several physical lines.
.SS GENERAL DIRECTIVES
These directives specify general configuration details.  They should
appear before directives specifying zones, as each will affect only
later zone directives.  Foreign zones (zones explicitly specified on
the command line but not mentioned in the configuration) use the
configuration settings prevailing at the end of the config file.
.TP
\fBadmin\fP \fIemail\-address\fP
Specifies the email address of the local administrator.  This is used
in the From: line of mails sent out, and will also receive copies of
the reports.  There is no default.
.TP
\fBdefault\-dir\fP \fIdirectory\fP
Makes
.I directory
be the default directory (which affects the interpretation of
relative filenames).  The default is the directory containing
the main configuration file, ie
.BR /etc/bind
if no
.B -C
option is specified.
.TP
\fBforbid\-addr\fP [\fIip-address ...\fP]
Specifies the list of addresses that are forbidden as any nameserver
for any zone.  The default is no such addresses.
.TP
\fBforbid\-addr\fP [\fIip-address ...\fP]
Specifies the list of addresses that are forbidden as a nameserver
for a zone for which we are the primary - ie, the list of our old or
to-be-obsoleted slaves.  The default is no such addresses.
.TP
\fBserverless\-glueless\fP \fIdomain ...\fP
Specifies a list of domains under which we do not expect to find any
nameservers without glue; for these zones it is OK to find glueless
referrals.
Each domain listed names a complete subtree of the DNS, starting at
the named point.  The default is
.BR "in\-addr.arpa ip6.arpa ip6.int" .

To avoid indefinitely long or even circularly glueless referrals
(which delay or prevent lookups) it is necessary for all sites to
effectively implement similar conventions; currently the author
believes that only the reverse lookup namespaces are conventionally
devoid of (glueless) nameservers, and therefore fine to provide
glueless referrals for.  See GLUELESSNESS below.
.TP
\fBallow-\-indirect\-glue\fP \fInameserver-superdomain ...\fP
Specifies a list of domains under which we expect to find glueless
nameservers, with up to one layer of indirection.
For nameservers under these domains it is OK to to find glueless
referrals, but only when listed as a nameserver for a zone which is
not itself a subdomain of an \fBallow-indirect-glue\fR
\fInameserver-superdomain\fR.

This supports to common configuration style where DNS operator(s) set
up all of their nameservers with names within a small subsection of
the DNS (the portions under \fInameserver-superdomain\fRs), and
provide glueless referrals naming these nameservers for all other
zones.  This provides at most one level of missing glue.

Note that if the DNS administrators collectively able to influence the
service for some zone (including the admins for its superzones, the
zones containing its nameservers, and their superzones and so forth)
are not in sufficiently close communication do not all agree on the
proper set of \fInameserver-superdomain\fR then they might still set
up circular glue and \fBchiark-named-conf\fR would not necessarily be
able to detect this even if it was run on every relevant nameserver.
.TP
\fBmail\-state\-dir\fP \fIdirectory\fP
Uses
.I directory
for storing information about recent failures for mailing to zone
admins.  See \-\-mail\-first et al.  Old files in here should be
cleaned up periodically out of cron.  There is no default.
.TP
\fBmail\-max\-warnfreq\fP \fIpercentage\fP
When \-\-mail\-final is used, a mail will be sent to all zones which
had warnings or errors more than
.IR percentage %
of the times \-\-mail\-* was used (since the last \-\-mail\-first).
The default is 50%.
.TP
.BR modifiers " " !*$@~? "] [\fIgroup\fP]"
Applies the specified zone style modifiers (see below) to subsequently
declared zones (until the next
.B modifiers
directive), as if the modifiers specified were written out for
each zone.  You must specify at least one character for the modifiers;
if you want to reset everything to the default, just say
.BR ! .
If style modifiers specified in the zone directive
conflict with the
.B modifiers
directive, those specified in the zone directive take effect.
.I group
may contain alphanumerics and underscores, and is used for the
.B -m
command-line option.
.TP
\fBself\-addr\fP \fIip-address ...\fP
Specifies the list of addresses that this server may be known by in
A records.  There is no default.
.TP
\fBoutput\fP \fIformat\fP \fIfilename\fP [\fIformat\fP \fIfilename ...\fP]
Arranges that each
.I filename
will be overwritten when
.BR -y " or " -f
are used; its new contents will be configuration
directives for the zones which follow for the
nameserver in question.  Currently the only
.I format
supported is
.B bind8
which indicates new-style BIND 8.  If no zones follow, then each
file will still be overwritten, by an effectively empty file.
Default: if there is no
.B output
directive in the configuration then the default is to use
.BR bind8 " " chiark-conf-gen.bind8 ;
otherwise it is an error for there to be any zones in the
configuration before the first
.B output
directive.
.TP
\fBself\-ns\fP \fIfqdn ...\fP
Specifies the list of names that this server may be known by in NS
records.  There is no default.  Any trailing * is replaced by the name
of the zone being checked, so for example
.B self\-ns isp.ns.*
before the zone example.com would mean to expect us to be listed as
isp.ns.example.com
in the NS RRset.
.TP
\fBself\-soa\fP \fIfqdn ...\fP
Specifies the list of names that this server may be known by in
the ORIGIN field of SOA records.  There is no default.  Any trailing
* is replaced by the name of the zone, as for
.BR self\-ns .
.TP
.BI self " fqdn ..."
Equivalent to both
.B self\-ns " and " self\-soa
with the same set of names.
.TP
\fBslave\-dir\fP \fIdirectory\fP [[\fIprefix\fP] \fIsuffix\fP]
Specifies the directory in which slave (published and stealth)
zonefiles should be placed.  The default
.I directory
is
.BR /var/cache/bind/chiark-slave .
The default
.IR suffix " and " prefix
are empty; they also will be reset to these defaults by a
.B slave\-dir
directive which does not specify them.
.SS ZONE DIRECTIVES
These directives specify one or more zones.
.TP
.BR primary [ !*$@~? "] \fIzone filename\fP"
Specifies that this server is supposed to be the primary nameserver
for
.I zone
and that the zone data is to be found in
.IR filename .
.TP
.BR primary\-dir [ !*$@~? "] \fIdirectory\fP[" / "\fIprefix\fP] [\fIsuffix\fP[" / \fIsubfile\fP]]
Search
.I directory
for files whose names start with
.I prefix
and end with
.IR suffix .
Each such file is taken to represent a zone file for which this server
is supposed to be the primary; the part of the filename between
.IR prefix " and " suffix
is the name of the zone.

If
.BI / subfile
is specified, then instead of looking for files, we search for
directories containing
.IR subfile ;
directories which do not contain the subfile are simply skipped.

If
.IR directory [\fB/\fP prefix ]
exists as specified and is a directory then it is interpreted as
.I directory
with an empty prefix; otherwise the final path component is assumed to
be the prefix.  If no
.IB suffix / subfile
is specified then the default is
.BR _db .
.TP
.BR published [ !*$@~? "] \fIzone origin\-addr\fP"
Specifies that this server is supposed to be a published slave
nameserver for the zone in question.
.TP
.BR stealth [ !*$@~? "] \fIzone server\-addr ...\fP"
Specifies that this server is supposed to be an unpublished secondary
(aka stealth secondary) for the zone in question.
.SS ZONE STYLE MODIFIERS
Each of the zone directives may optionally be followed by one or more
of the following characters (each at most once):
.TP
.B !
Reverses the meaning of all style modifiers after the
.BR ! .
Only one
.BR !
must appear in the modifier list.  In this list, other modifiers which
default to `enabled' are described by describing the effect of their
inverse - see the description for
.B !@
below.
.TP
.B *
Indicates that the zone is unofficial, ie that it is not delegated as
part of the global Internet DNS and that no attempt should be made to
find the superzone and check delegations.  Note that unofficial, local
zones should be created with caution.  They should be in parts of the
namespace which are reserved for private use, or belong to the actual
zone maintainer.
.TP
.B $
Indicates that any mails should be sent about the zone to the
nameserver admin rather than to the zone SOA MNAME.  This is the
default unless we are supposedly a published server for the zone.
.TP
.B !@
Indicates that no mails should be sent about the zone to anyone.
.TP
.B ~
Indicates that the zone's delegation is known to be glueless, and that
lack of glue should not be flagged.  Not recommended - see the section
GLUELESSNESS, below.
.TP
.B ?
Indicates that the zone is known to be broken and no checks should be
carried out on it, unless the
.B \-A
option is specified.
.SS OTHER DIRECTIVES
.TP
\fBinclude\fP \fIfile\fP
Reads
.I file
as if it were included here.
.TP
\fBend\fP
Ends processing of this file; any data beyond this point is ignored.
.SH CHECKS
chiark\-named\-conf makes the following checks:

Delegations: Each delegation from a server for the superzone should
contain the same set of nameservers.  None of the delegations should
lack glue.  The glue addresses should be the same in each delegation,
and agree with the local default nameserver.

Delegated servers: Each server mentioned in the delegation should have
the same SOA record (and obviously, should be authoritative).

All published nameservers - including delegated servers and servers
named in the zone's nameserver set: All nameservers for the zone
should supply the same list of nameservers for the zone, and none of
this authority information should be glueless.  All the glue should
always give the same addresses.

Origin server's data: The set of nameservers in the origin server's
version of the zone should be a superset of those in the delegations.

Our zone configuration: For primary zones, the SOA origin should be
one of the names specified with
.BR self\-soa " (or " self ).
For published zones, the address should be that of the SOA origin.
For stealth zones, the address should be that of the SOA origin or one
of the published nameservers.
.SH GLUELESSNESS
Glue is the name given for the addresses of nameservers which are
often supplied in a referral.  In fact, it turns out that it is
important for the reliability and performance of the DNS that
referrals, in general, always come with glue.

Firstly, glueless referrals usually cause extra delays looking up
names.  BIND 8, when it receives a completely glueless referral and
does not have the nameservers' addresses in its cache, will start
queries for the nameserver addresses; but it will throw the original
client's question away, so that when these queries arrive, it won't
restart the query from where it left off.  This means that the client
won't get its answer until it retries, typically at least 1 second
later - longer if you have more than one nameserver listed.  Worse, if
the nameserver to which the glueless referral points is itself under
another glueless referral, another retry will be required.

Even for better resolvers than BIND 8, long chains of glueless
referrals can cause performance and reliability problems, turning a
simple two or three query exchange into something needing more than a
dozen queries.

Even worse, one might accidentally create a set of circularly glueless
referrals such as
.br
.B example.com NS ns0.example.net.uk
.br
.B example.com NS ns1.example.net.uk
.br
.B example.net.uk NS ns0.example.com
.br
.B example.net.uk NS ns1.example.com
.br
Here it is impossible to look up anything in either example.com or
example.net.uk.

There are, as far as the author is aware, no generally agreed
conventions or standards for avoiding unreasonably long glueless
chains, or even circular glueless situations.  The only way to
guarantee that things will work properly is therefore to always supply
glue.

However, the situation is further complicated by the fact that many
implementations (including BIND 8.2.3, and many registry systems),
will refuse to accept glue RRs for delegations in a parent zonefile
unless they are under the parent's zone apex.  In these cases it can
be necessary to create names for the child's nameservers which are
underneath the child's apex, so that the glue records are both in the
parent's bailiwick and obviously necessary.

In the past, the `shared registry system' managing .com, .net and .org
did not allow a single IPv4 address to be used for more than one
nameserver name.  However, at the time of writing (October 2002) this
problem seems to have been fixed, and the workaround I previously
recommended (creating a single name for your nameserver somewhere
in .com, .net or .org, and using that for all the delegations
from .com, .net and .org) should now be avoided.

Finally, a note about `reverse' zones, such as those in in-addr.arpa:
It does not seem at all common practice to create nameservers in
in-addr.arpa zones (ie, no NS RRs seem to point into in-addr.arpa,
even those for in-addr.arpa zones).  Current practice seems to be to
always use nameservers for in-addr.arpa which are in the normal,
forward, address space.  If everyone sticks to the rule of always
publishing nameservers names in the `main' part of the namespace, and
publishing glue for them, there is no chance of anything longer than a
1-step glueless chain might occur for a in-addr.arpa zone.  It is
probably best to maintain this as the status quo, despite the
performance problem this implies for BIND 8 caches.  This is what the
serverless\-glueless directive is for.

Dan Bernstein has some information and examples about this at
.UR http://cr.yp.to/djbdns/notes.html#gluelessness
http://cr.yp.to/djbdns/notes.html#gluelessness
.UE
but be warned that it is rather opinionated.
.SS GLUELESSNESS SUMMARY

I recommend that every nameserver should have its own name in every
forward zone that it serves.  For example:
.br
.B zone.example.com NS servus.ns.example.com
.br
.B servus.ns.example.com A 127.0.0.2
.br
.B 2.0.0.127.in-addr.arpa PTR servus.example.net
.br
.B servus.example.net A 127.0.0.2
.LP
Domain names in
.B in-addr.arpa
should not be used in the right hand side of NS records.
.SH SECURITY
chiark\-named\-conf is supposed to be resistant to malicious data in
the DNS.  It is not resistant to malicious data in its own options,
configuration file or environment.  It is not supposed to read its
stdin, but is not guaranteed to be safe if stdin is dangerous.
.LP
Killing chiark-named-conf suddenly should be safe, even with
.BR -y " or " -f
(though of course it may not complete its task if killed), provided
that only one invocation is made at once.
.LP
Slow remote nameservers will cause chiark-named-conf to take
excessively long.
.SH EXIT STATUS
.TP
.B 0
All went well and there were no warnings.
.TP
any other
There were warnings or errors.
.SH FILES
.TP
.B /etc/bind/chiark-conf-gen.zones
Default input configuration file.  (Override with
.BR -C .)
.TP
.B /etc/bind
Default directory.  (Override with
.BR -C " or " default\-dir .)
.TP
.IB dir /chiark-conf-gen.bind8
Default output file.
.TP
.B /var/cache/bind/chiark-slave
Default location for slave zones.
.SH ENVIRONMENT
.LP
Setting variables used by
.BR dig (1)
and
.BR adnshost (1)
will affect the operation of chiark\-named\-conf.  
Avoid messing with these if possible.
.LP
.B PATH
is used to find subprograms such as
.BR dig " and " adnshost .
.SH BUGS
The determination of the parent zone for each zone to be checked, and
its nameservers, is done simply using the system default nameserver.

The processing of output from
.B dig
is not very reliable or robust, but this is mainly the fault of dig.
This can lead to somewhat unhelpful error reporting for lookup
failures.
.SH AUTHOR
.B chiark\-named\-conf
and this manpage were written by Ian Jackson
<ian@chiark.greenend.org.uk>.  They are Copyright 2002 Ian Jackson.

chiark\-named\-conf and this manpage are free software; you can
redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation; either
version 2, or (at your option) any later version.

This is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along
with this program; if not, consult the Free Software Foundation's
website at www.fsf.org, or the GNU Project website at www.gnu.org.
